import java.util.HashMap;

public class test {
    public static void main(String[] args) {
        HashMap n1 = new HashMap();
        n1.put(100, "Thanwa");
        n1.put(50, "Saensud");

        System.out.println(n1.get(100));
        System.out.println(n1.get(50));

        n1.remove(100);
        n1.remove(50);
        
        System.out.println(n1.get(100));
        System.out.println(n1.get(50));
    }
}
