public class HashMap {

    private int key;
    private String value;
    private int index;
    private String[] lookUpTable = new String[23];

    public HashMap(int key, String value) {
        this.key = key;
        this.value = value;
        this.lookUpTable[hash(key)] = value;
    }

    public int hash(int key) {
        return key % lookUpTable.length;
    }

    public void put(int key, String value) {
        lookUpTable[hash(key)] = value;
    }

    public String get(int key) {
        return (String) lookUpTable[hash(key)];
    }

    public String remove(int index){
        String temp =  lookUpTable[hash(index)];
        lookUpTable[hash(index)]=null;
        return temp;
    }
}